FROM golang:1.21.5-alpine AS builder

WORKDIR /app
ADD . /app

COPY go.mod ./

RUN go mod download && \
    go mod verify

COPY . ./

RUN go build -o main .


FROM scratch

COPY --from=builder /app/main .

ENTRYPOINT ["./main"]